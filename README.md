### Install dependencies
```
npm install
```
or
```
yarn install
```

### Build app
```
npm run build
```

### app to webview
**TODO: need CI/CD, environments, merge react app with apk (React Native | Native code?)** 
move all from `build/`, then paste into `evotor-app-apk/app/src/main/assets/views/udsgame`
```
mv ~/<pathToCode>/evotor-app-web/build ~/<pathToCode>/evotor-app-apk/app/src/main/assets/views/udsgame
``` 

or create a symlink

```
mv ~/<pathToCode>/evotor-app-web/build ~/<pathToCode>/evotor-app-web/udsgame
ln -s ~/<pathToCode>/evotor-app-web/udsgame ~/<pathToCode>/evotor-app-apk/app/src/main/assets/views/
```
after creating rename 
