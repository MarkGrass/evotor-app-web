import {
    CHARGE_DISCOUNT_START, CLEAR_DISCOUNT_ERRORS, DISCOUNT_CHARGED, SCORES_VALUE_FAIL, UPDATE_SCORES
} from "../actions/actionTypes";

const defaultState = {
    loading: false,
    data: null,
    errors: null,
    scores: 0
};

export default function discount(state = defaultState, action) {
    switch (action.type) {

        case CHARGE_DISCOUNT_START:
            return {
                ...state,
                loading: true
            };

        case DISCOUNT_CHARGED:
            return {
                ...state,
                loading: false,
                data: null,
                errors: null
            };

        case UPDATE_SCORES:
            return {
                ...state,
                scores: Math.abs(action.scores)
            };

        case SCORES_VALUE_FAIL:
            return {
                ...state,
                loading: false,
                data: null,
                errors: action.errors
            };

        case CLEAR_DISCOUNT_ERRORS:
            return {
                ...state,
                loading: false,
                data: null,
                errors: null
            };

        default:
            return state;
    }
}