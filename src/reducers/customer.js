import {
    CLEAR_ERRORS, LOAD_INFO_FAIL, LOAD_INFO_OK, LOAD_INFO_REQUESTED, UPDATE_IDENTIFIER,
    VALUE_FAIL
} from "../actions/actionTypes";

const defaultState = {
    loading: false,
    info: null,
    errors: null,
    identifier: null
};

export default function customer(state = defaultState, action) {
    switch (action.type) {

        case LOAD_INFO_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case UPDATE_IDENTIFIER:
            return {
                ...state,
                identifier: action.identifier
            };

        case CLEAR_ERRORS:
            return {
                ...state,
                loading: false,
                info: null,
                errors: null
            };

        case VALUE_FAIL:
            return {
                ...state,
                loading: false,
                info: null,
                errors: action.errors
            };

        case LOAD_INFO_OK:
            return {
                ...state,
                info: action.info,
                errors: null
            };

        case LOAD_INFO_FAIL:
            return {
                ...state,
                loading: false,
                info: null,
                errors: action.errors
            };

        default:
            return state;
    }
}