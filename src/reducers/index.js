import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import evotor from '../reducers/evotor';
import customer from '../reducers/customer';
import company from '../reducers/company';
import discount from "../reducers/discount";

export default combineReducers({
    routing: routerReducer,
    evotor,
    customer,
    company,
    discount
});