import {
    CLEAR_ERRORS,
    LOAD_COMPANY_INFO_FAIL,
    LOAD_COMPANY_INFO_OK,
    LOAD_COMPANY_INFO_REQUESTED
} from "../actions/actionTypes";

const defaultState = {
    loading: false,
    companyInfo: null,
    errors: null,
};

export default function company(state = defaultState, action) {
    switch (action.type) {

        case LOAD_COMPANY_INFO_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case LOAD_COMPANY_INFO_OK:
            return {
                ...state,
                loading: false,
                companyInfo: action.companyInfo,
                errors: null
            };

        case LOAD_COMPANY_INFO_FAIL:
            return {
                ...state,
                loading: false,
                companyInfo: null,
                errors: action.errors
            };

        case CLEAR_ERRORS:
            return {
                ...state,
                loading: false,
                companyInfo: null,
                errors: null
            };

        default:
            return state;
    }
}