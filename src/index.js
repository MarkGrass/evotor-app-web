import React from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import {
    HashRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import {syncHistoryWithStore} from 'react-router-redux';
import {createHashHistory} from 'history';

import App from './containers/App/App';
import Discount from './containers/Discount/Discount';
import './index.css';
import reducers from './reducers'

// export default function promiseMiddleware() {
//
//     return next => action => {
//         const { promise, type, ...rest } = action;
//
//         if (!promise || rest.isFb) return next(action);
//
//         const SUCCESS = type + '_SUCCESS';
//         const REQUEST = type + '_REQUEST';
//         const FAILURE = type + '_FAILURE';
//         next({...rest, type: REQUEST});
//         return promise
//             .then(req => {
//                 next({...rest, req, type: SUCCESS});
//                 return true;
//             })
//             .catch(error => {
//                 next({...rest, error, type: FAILURE});
//                 console.log(error);
//                 return false;
//             });
//     };
// }

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));
const history = syncHistoryWithStore(createHashHistory(), store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <div>
                <Switch>
                    <Route exact path="/" component={App}/>
                    <Route path={'/discount'} component={Discount}/>
                </Switch>
            </div>
        </Router>
    </Provider>,
    document.getElementById('root')
);
