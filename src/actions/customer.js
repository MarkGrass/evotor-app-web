import axios from 'axios';
import {isValidNumber} from 'libphonenumber-js'
import {LOAD_INFO_FAIL, LOAD_INFO_OK, LOAD_INFO_REQUESTED, VALUE_FAIL} from "./actionTypes";

export function getCustomer(identifier) {
    return dispatch => {
        dispatch({
            type: LOAD_INFO_REQUESTED
        });

        if (!identifier) {
            return Promise.reject(
                dispatch({
                    type: VALUE_FAIL,
                    errors: {
                        message: 'Поле не может быть пустым'
                    }
                })
            );
        }

        return new Promise((res, rej) => {
            axios.get('https://evotor.uds.app/secure/partner/api/v1/customer',
                {
                    params: identifier
                })
                .then(result => {
                    res(dispatch({
                        type: LOAD_INFO_OK,
                        info: result.data
                    }));
                })
                .catch(error => {
                    if (!error.response || !error.response.data.message) {
                        rej(dispatch({
                            type: LOAD_INFO_FAIL,
                            errors: {
                                message: 'Ошибка сервера. Попробуйте еще раз позже'
                            }
                        }));
                    } else {
                        rej(dispatch({
                            type: LOAD_INFO_FAIL,
                            errors: error.response.data
                        }))
                    }
                })
        })
    }
}

export function validateNumber(phoneNumber) {
    return dispatch => {
        return new Promise((res, rej) => {
            if (isValidNumber(phoneNumber)) {
                res();
            } else {
                rej(dispatch({
                    type: VALUE_FAIL,
                    errors: {
                        message: 'Введите корректный номер телефона'
                    }
                }));
            }
        });
    }
}
