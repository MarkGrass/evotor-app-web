import axios from 'axios';
import {LOAD_COMPANY_INFO_FAIL, LOAD_COMPANY_INFO_OK, LOAD_COMPANY_INFO_REQUESTED} from "./actionTypes";

export function getCompanyInfo() {
    return dispatch => {
        dispatch({
            type: LOAD_COMPANY_INFO_REQUESTED
        });

        return new Promise((res, rej) => {
            axios.get('https://evotor.uds.app/secure/partner/api/v1/company')
                .then(
                    (result) => {
                        res(dispatch({
                            type: LOAD_COMPANY_INFO_OK,
                            companyInfo: result.data
                        }));
                    })
                .catch(
                    (error) => {
                        if (!error.response || !error.response.data.message) {
                            rej(dispatch({
                                type: LOAD_COMPANY_INFO_FAIL,
                                errors: {
                                    message: 'Ошибка сервера. Попробуйте еще раз позже'
                                }
                            }));
                        } else {
                            rej(dispatch({
                                type: LOAD_COMPANY_INFO_FAIL,
                                errors: error.response.data
                            }))
                        }
                    })
        })
    }
}
