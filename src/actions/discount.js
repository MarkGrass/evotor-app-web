import {CHARGE_DISCOUNT_START, DISCOUNT_CHARGED, SCORES_VALUE_FAIL} from "./actionTypes";

export function runLoader() {
    return dispatch => {
        dispatch({
            type: CHARGE_DISCOUNT_START
        });
    }
}

export function discountCharged() {
    return dispatch => {
        dispatch({
            type: DISCOUNT_CHARGED
        });
    }
}

export function scoresValueFailed() {
    return dispatch => {
        dispatch({
            type: SCORES_VALUE_FAIL,
            errors: {
                message: 'Указано неверное количество баллов для списания'
            }
        });
    }
}