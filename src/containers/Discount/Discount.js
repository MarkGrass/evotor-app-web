import React from 'react';
import {connect} from 'react-redux';
import Profile from "../../components/common/Profile"
import Input from "../../components/common/Input"
import Button from "../../components/common/Button"
import Loader from "../../components/common/Loader";
import Modal from "../../components/common/Modal";
import Decimal from 'decimal.js';
import './Discount.css';
import {CLEAR_DISCOUNT_ERRORS, UPDATE_SCORES} from "../../actions/actionTypes";
import {discountCharged, runLoader, scoresValueFailed} from "../../actions/discount";
import ProfilePhone from "../../components/common/ProfilePhone/index";

class Discount extends React.Component {

    getTotalSum() {
        return parseFloat(JSON.parse(this.props.evotor.receipt.getReceipt()).receiptData.totalSumWithoutDiscount);
    }

    getTotalSumWithDiscount(discount) {
        const baseDiscountPolicy = this.props.company.companyInfo.baseDiscountPolicy;
        const discountRate = (baseDiscountPolicy === 'CHARGE_SCORES') ? 0 : discount;
        const totalSum = this.getTotalSum();
        return new Decimal(totalSum - (totalSum / 100) * discountRate).toFixed(2);
    }

    getTotalSumWithDiscountAndScores(discount) {
        return this.getTotalSumWithDiscount(discount) - new Decimal(this.props.discount.scores).toFixed(2);
    }

    chargeDiscount() {
        this.props.onRunLoader();
        const maxScores = +new Decimal((this.getTotalSumWithDiscount(this.props.customer.info ? this.props.customer.info.discountRate : 0) / 100) * this.props.company.companyInfo.marketingSettings.maxScoresDiscount).toFixed(2);
        if (this.props.discount.scores && this.props.discount.scores > (maxScores > this.props.customer.info.scores ? this.props.customer.info.scores : maxScores)) {
            this.props.onScoresValueFailed();
        } else {
            let cash = this.getTotalSumWithDiscountAndScores(this.props.customer.info ? this.props.customer.info.discountRate : 0);
            const discount = +new Decimal(this.getTotalSum() - Math.abs(cash)).toFixed(2);
            const identifier = (this.props.customer.identifier.code) ? {customerId: this.props.customer.info.id} : this.props.customer.identifier;

            const purchaseData = {
                ...identifier,
                total: +new Decimal(this.getTotalSum()).toFixed(2),
                cash: +new Decimal(cash).toFixed(2),
                scores: +new Decimal(this.props.discount.scores).toFixed(2)
            };

            this.props.evotor.receipt.applyReceiptDiscount(discount);
            this.props.evotor.receipt.addReceiptExtra(JSON.stringify(purchaseData));

            this.props.onDiscountCharged();
            this.elProfile.focus();
            this.props.evotor.navigation.pushNext();
        }
    }

    updateInputValue(evt) {
        this.props.onUpdateScores(evt.target.value);
    }

    closeModal() {
        this.props.onCloseModal();
    }

    render() {
        const {info} = this.props.customer;
        const {companyInfo} = this.props.company;
        const {loading, errors} = this.props.discount;

        return (
            <div>

                {(loading) ? <Loader/> : ''}
                {(errors) ? <Modal text={errors.message} onClick={() => this.closeModal()}/> : ''}

                {
                    info ?
                        <Profile scores={info.scores}
                                 baseDiscountPolicy={companyInfo.baseDiscountPolicy}
                                 maxScoresDiscount={companyInfo.marketingSettings.maxScoresDiscount}
                                 discount={info.discountRate}
                                 name={info.name}
                                 surname={info.surname}
                                 vip={info.vip}
                                 participant={info.participant}
                                 ref={(Profile) => this.elProfile = Profile}
                                 totalPrice={+new Decimal(this.getTotalSum()).toFixed(2)}
                                 totalPriceWithDiscount={+new Decimal(this.getTotalSumWithDiscount(info.discountRate)).toFixed(2)}
                                 totalPriceWithDiscountAndScores={+new Decimal(this.getTotalSumWithDiscountAndScores(info.discountRate)).toFixed(2)}/>
                        :
                        <ProfilePhone baseDiscountPolicy={companyInfo.baseDiscountPolicy}
                                      discount={companyInfo.marketingSettings.discountBase}
                                      totalPrice={+new Decimal(this.getTotalSum()).toFixed(2)}
                                      name={this.props.customer.identifier.phone}/>

                }

                <form onSubmit={() => this.chargeDiscount()}>
                    {
                        (info && info.scores && companyInfo.marketingSettings.maxScoresDiscount) ?
                            <div>
                                <div className="content-padding input-block">
                                    <Input type="number"
                                           placeholder={'Введите сумму баллов'}
                                           onChange={evt => this.updateInputValue(evt)}/>
                                </div>
                            </div>
                            : ''
                    }

                    <div className="button-block content-padding">
                        <Button text={'Готово'}
                                actionStyle={true}
                                onClick={() => this.chargeDiscount()}/>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        evotor: state.evotor,
        customer: state.customer,
        company: state.company,
        discount: state.discount
    }),
    (dispatch) => ({
        onRunLoader: () => {
            dispatch(runLoader());
        },
        onDiscountCharged: () => {
            dispatch(discountCharged());
        },
        onScoresValueFailed: () => {
            dispatch(scoresValueFailed());
        },
        onCloseModal: () => {
            dispatch({type: CLEAR_DISCOUNT_ERRORS});
        },
        onUpdateScores: (value) => {
            dispatch({type: UPDATE_SCORES, scores: value});
        }
    })
)(Discount);
