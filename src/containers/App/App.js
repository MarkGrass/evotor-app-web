import React from 'react';
import {connect} from 'react-redux';

import {CLEAR_ERRORS, UPDATE_IDENTIFIER} from "../../actions/actionTypes";
import {getCustomer, validateNumber} from '../../actions/customer';
import {getCompanyInfo} from "../../actions/company";

import Receipt from "../../components/common/Receipt";
import Button from "../../components/common/Button";
import Loader from "../../components/common/Loader";
import Input from "../../components/common/Input";
import Modal from "../../components/common/Modal";


import './App.css';

class App extends React.Component {

    get totalSum() {
        return JSON.parse(this.props.evotor.receipt.getReceipt()).receiptData.totalSumWithoutDiscount;
    }

    getCustomerInfo() {
        this.props.onGetCustomer(this.props.customer.identifier).then((customerInfo) => {
            this.props.evotor.logger.log('Customer info ', customerInfo);

            this.props.onGetCompanyInfo().then((companyInfo) => {
                this.props.evotor.logger.log('Company info ', companyInfo);
                this.props.history.push('/discount');
            }).catch((err) => {
                this.props.evotor.logger.log('Error company', err);
            });
        }).catch((err) => {
            this.props.evotor.logger.log('Error customer', err);
        });
    }

    close() {
        this.props.evotor.navigation.pushNext();
    }

    updateInputValue(type, value) {
        this.props.onUpdateIdentifier({[type]: value});
    }

    validatePhoneNumber() {
        this.props.onValidateNumber(this.props.customer.identifier.phone).then(
            () => {
                this.props.onGetCompanyInfo().then((companyInfo) => {
                    this.props.evotor.logger.log('Company info ', companyInfo);
                    this.props.history.push('/discount');
                }).catch((err) => {
                    this.props.evotor.logger.log('Error company', err);
                })
            }
        );
    }

    closeModal() {
        this.props.onCloseModal();
    }

    render() {
        const loading = this.props.customer.loading || this.props.company.loading;
        const errors = this.props.customer.errors || this.props.company.errors;

        const divStyle = {
            fontSize: '18px',
            position: 'absolute',
            paddingTop: '15px',
            paddingLeft: '15px'
        };

        const inputStyle = {
            paddingLeft: '35px'
        };

        return (
            <div>
                {(loading) ? <Loader/> : ''}

                {(errors) ? <Modal text={errors.message} onClick={() => this.closeModal()}/> : ''}

                <Receipt totalSum={this.totalSum}/>

                <form>
                    <div className="tabs">
                        <input id="tab1" type="radio" name="tabs" defaultChecked/>
                        <label htmlFor="tab1">Использовать код</label>

                        <input id="tab2" type="radio" name="tabs"/>
                        <label htmlFor="tab2">По номеру телефона</label>

                        <section id="content-tab1">
                            <div className="content-padding">
                                <Input placeholder={'Введите код скидки'}
                                       type="number"
                                       onChange={evt => this.updateInputValue('code', evt.target.value)}/>
                            </div>

                            <div className="button-block content-padding">
                                <Button text={'Продолжить'}
                                        actionStyle={true}
                                        onClick={() => this.getCustomerInfo()}/>
                            </div>
                        </section>
                        <section id="content-tab2">
                            <div className="content-padding by-phone">
                                <div style={divStyle}>+7</div>
                                <Input placeholder={'Введите номер телефона'}
                                       style={inputStyle}
                                       type="number"
                                       onChange={evt => this.updateInputValue('phone', '+7' + evt.target.value)}/>
                            </div>
                            <div className="button-block content-padding">
                                <Button text={'Продолжить'}
                                        actionStyle={true}
                                        onClick={() => this.validatePhoneNumber()}/>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        evotor: state.evotor,
        company: state.company,
        customer: state.customer
    }),
    (dispatch) => ({
        onGetCustomer: (value) => {
            return dispatch(getCustomer(value));
        },
        onValidateNumber: (phoneNumber) => {
            return dispatch(validateNumber(phoneNumber));
        },
        onGetCompanyInfo: () => {
            return dispatch(getCompanyInfo());
        },
        onCloseModal: () => {
            dispatch({type: CLEAR_ERRORS});
        },
        onUpdateIdentifier: (value) => {
            dispatch({type: UPDATE_IDENTIFIER, identifier: value});
        }
    })
)(App);
