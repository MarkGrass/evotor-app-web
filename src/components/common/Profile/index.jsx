import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Decimal from 'decimal.js';

import './profile.css';

const propTypes = {
    scores: PropTypes.number.isRequired,
    baseDiscountPolicy: PropTypes.string,
    maxScoresDiscount: PropTypes.number,
    discount: PropTypes.number.isRequired,
    totalPrice: PropTypes.number.isRequired,
    totalPriceWithDiscount: PropTypes.number.isRequired,
    totalPriceWithDiscountAndScores: PropTypes.number.isRequired,
    name: PropTypes.string,
    surname: PropTypes.string,
    participant: PropTypes.bool,
    vip: PropTypes.bool
};

const defaultProps = {
    vip: false
};

class Profile extends React.Component {

    renderUserName(name, surname) {
        if (name) {
            return (
                <p className="name">{name} {surname}</p>
            )
        }
    }

    render() {
        const {
            scores,
            baseDiscountPolicy,
            maxScoresDiscount,
            discount,
            totalPrice,
            totalPriceWithDiscount,
            totalPriceWithDiscountAndScores,
            name,
            surname,
            participant,
            vip
        } = this.props;

        const discountInfoClasses = classNames({
            'discount-info': true,
            'vip': vip
        });

        const iconClasses = classNames({
            'icon': true,
            'participant': !vip && participant,
            'vip': vip,
            'none': !(vip || participant)
        });

        const labelIcon = classNames({
            'label': true,
            'vip': vip
        });

        const maxScores = +new Decimal((totalPriceWithDiscount / 100) * maxScoresDiscount).toFixed(2);

        return (
            <div id="main" className="profile">
                <div className="user-info">
                    {this.renderUserName(name, surname)}
                    {
                        (!participant) ?
                            <div className="status">
                                <div className={iconClasses}/>

                                <div className={labelIcon}>Клиента нет в базе системы лояльности</div>
                            </div>
                            : ''
                    }
                </div>
                <div className={discountInfoClasses}>
                    <div className="discount">
                        {vip ? 'vip' : ''} {(baseDiscountPolicy === 'CHARGE_SCORES') ? 'CASHBACK' : 'СКИДКА'} {discount}%
                    </div>

                    <div className="total-sum">
                        <div className="caption">к оплате с учетом скидки</div>
                        <div className="value">{totalPriceWithDiscountAndScores}</div>
                    </div>
                </div>
                {
                    (scores && maxScoresDiscount) ?
                        <div className="scores-info">
                            <div className="caption">Баллов у клиента: {scores}</div>
                            <div className="value">
                                <div className="scores">Доступно для списания:</div>
                                <div className="avatar"/>
                                <div
                                    className="scores">{maxScores > scores ? scores : maxScores}</div>
                            </div>
                            <div className="description">Вы можете списать баллы для частичного или полного погашения
                                суммы счета
                            </div>
                        </div>
                        : ''
                }
            </div>
        );
    }
}

Profile.propTypes = propTypes;
Profile.defaultProps = defaultProps;

export default Profile;