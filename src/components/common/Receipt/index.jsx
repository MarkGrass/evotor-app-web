import React from 'react';
import PropTypes from 'prop-types'

import './receipt.css';

const propTypes = {
    totalSum: PropTypes.string.isRequired
};

class Receipt extends React.Component {
    render() {
        const {
            totalSum
        } = this.props;

        return (
            <div className="receipt">
                <div className="row">
                    <div className="caption">к оплате</div>
                </div>
                <div className="row">
                    <div className="value">{totalSum}</div>
                </div>
            </div>
        );
    }
}

Receipt.propTypes = propTypes;

export default Receipt;