import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './button.css'

const propTypes = {
    actionStyle: PropTypes.bool,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

const defaultProps = {
    actionStyle: false
};

class Button extends React.Component {
    render() {
        const {
            text,
            onClick,
            actionStyle
        } = this.props;

        const buttonClasses = classNames({
            'button': true,
            'action': actionStyle
        });

        return <div className={buttonClasses} onClick={onClick}>{text}</div>;
    }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;