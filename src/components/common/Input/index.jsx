import React from 'react';
import PropTypes from 'prop-types'

import './input.css';

const propTypes = {
    style: PropTypes.object,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    onKeyUp: PropTypes.func,
    onChange: PropTypes.func
};

const defaultProps = {
    type: 'text'
};

class Input extends React.Component {
    render() {
        const {
            type,
            style,
            placeholder,
            onChange,
            onKeyUp
        } = this.props;

        return (
            <div className="custom-input">
                <input className="input" style={style} type={type} placeholder={placeholder} onKeyUp={onKeyUp} onChange={onChange}/>
            </div>
        );
    }
}

Input.propTypes = propTypes;
Input.defaultProps = defaultProps;

export default Input;