import React from 'react';
import PropTypes from 'prop-types';

import './profilePhone.css';

const propTypes = {
    baseDiscountPolicy: PropTypes.string,
    discount: PropTypes.number.isRequired,
    totalPrice: PropTypes.number.isRequired,
    name: PropTypes.string,
};

class ProfilePhone extends React.Component {

    render() {
        const {
            baseDiscountPolicy,
            discount,
            totalPrice,
            name
        } = this.props;

        return (
            <div id="main" className="profile">
                <div className="user-info">
                    <p className="name">{name}</p>
                </div>
                <div className="discount-info">
                    <div className="discount">
                        {(baseDiscountPolicy === 'CHARGE_SCORES') ? 'CASHBACK ' + discount + '%' : ''}
                    </div>

                    <div className="total-sum">
                        <div className="caption">общая сумма к оплате</div>
                        <div className="value">{totalPrice}</div>
                    </div>
                </div>
            </div>
        );
    }
}

ProfilePhone.propTypes = propTypes;

export default ProfilePhone;