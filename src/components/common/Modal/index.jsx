import React from 'react';
import PropTypes from 'prop-types';

import './modal.css';

const propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

class Modal extends React.Component {
    render() {
        const {
            text,
            onClick
        } = this.props;

        return (
            <div className="overlay">
                <div className="modal-wrapper">
                    <div className="modal-content">
                        <p>{text}</p>
                    </div>
                    <div className="modal-button" onClick={ onClick }>Закрыть</div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = propTypes;

export default Modal;
